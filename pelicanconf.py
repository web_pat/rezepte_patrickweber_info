#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'Patrick Weber'
SITENAME = 'rezepte.patrickweber.info'
SITESUBTITLE = 'Nur Rezepte, keine Werbung'
SITEURL = 'https://rezepte.patrickweber.info'
PATH = 'content'
TIMEZONE = 'Europe/Tallinn'
DEFAULT_LANG = 'de'
DEFAULT_PAGINATION = 20
SUMMARY_MAX_LENGTH = 25

THEME = './theme/tuxlite_recipes/'
THEME_STATIC_DIR = 'theme'
THEME_STATIC_PATHS = ['static']
CSS_FILE = 'main.css'

DATE_FORMATS = {
    'en': '%a, %d %b %Y',
    'de': '%d.%m.%Y',
}

#ARTICLE_ORDER_BY = 'title'
DISPLAY_PAGES_ON_MENU = False
#MENUITEMS = (('Index', './index.html'),)

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('patrickweber.info', 'https://patrickweber.info'),
         ('based.cooking', 'https://based.cooking'),
        )

# Social widget
#SOCIAL = (('You can add links in your config file', '#'),
#          ('Another social link', '#'),)

# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = True

# static paths will be copied without parsing their contents
STATIC_PATHS = [
    'images',
    ]
