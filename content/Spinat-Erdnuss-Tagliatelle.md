Title: Tagliatelle in veganer Spinat-Erdnuss-Soße
Date: 2021-07-12
Modified: 2021-07-12
Category: Gericht
Tags: vegan, Pasta
Slug: spinat-erdnuss-tagliatelle
Authors: Patrick Weber
Lang: de

## Zutaten
* 250g Hartweizen-Tagliatelle
* Salz
* 2 Zwiebeln
* 2 Knoblauchzehen
* 200g Kirschtomaten
* 2 EL Pflanzenöl
* 400g frischer Blattspinat oder tiefgekühlt
* 150ml Sojasahne
* 150ml vegane Brühe
* 5 EL cremige Erdnußbutter
* 3-4 TL Limettensaft
* 1 Prise Zucker
* 1 Messerspitze gemahlener Muskat
* frisch gemahlener Pfeffer
* 2 EL geröstete, gesalzene Erdnüsse

## Zubereitung
Nudeln nach Packungsanweisung zubereiten. Zwiebeln und Knoblauch abziehen, Zwiebeln fein würfeln und Knoblauch zerdrücken. Tomaten waschen und vierteln. Spinat waschen, verlesen, mit Zwiebel und Knoblauch in erhitztem Öl circa 5 Minuten dünsten. Sahne und Brühe angießen, aufkochen und Erdnußbutter unter Rühren in der Soße schmelzen. Soße mit Limettensaft, Zucker, Muskat, Salz und Pfeffer abschmecken und mit den Nudeln vermischen. Erdnüsse grob hacken. Spinat-Erdnuss-Tagliatelle auf Tellern anrichten, mit Erdnüssen bestreuen und servieren.
