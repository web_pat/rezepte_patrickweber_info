Title: Italienische Nudelpfanne
Date: 2021-07-24
Modified: 2023-08-10
Category: Gericht
Tags: italienisch
Slug: italienische-nudelpfanne
Authors: Patrick Weber
Lang: de

## Zutaten für 4 Personen
* 300 g Nudeln
* Salz
* 2 kleine Zucchini
* 2 Tomaten
* 40 g getrocknete Tomaten
* 1 EL Olivenöl
* 4 EL rotes Pesto
* 50 g grüne Oliven, entsteint
* 2-3 Stiele Basilikum
* Pfeffer

## Zubereitung
Nudeln in kochendem Salzwasser nach Packungsanweisung zubereiten. Zucchini in schmale Scheiben schneiden. Tomaten halbieren und in Stücke schneiden. Getrocknete Tomaten in Streifen schneiden.

Nudeln abgießen und gut abtropfen lassen. Öl in einer Pfanne erhitzen und die Zucchinischeiben darin 2-3 Minuten unter Wenden anbraten. Nudeln und getrocknete Tomaten zugeben und ca. 2 Minuten mitbraten. Pesto, Oliven und Tomatenstücke untermischen und alles weitere ca. 3 Minuten braten. Basilikumblättchen von einem Stiel fein hacken und zu den Nudeln geben. Nudelpfanne mit Salz und Pfeffer abschmecken und mit Basilikumblättchen garniert servieren.

Zubereitungszeit ca. 20 Minuten. Pro Portion ca. 390kcal/1640kJ. E12g, F11g, KH 61g.

![Italienische Nudelpfanne](./images/Italienische_Nudelpfanne.webp)
