Title: Sangria
Date: 2021-07-24
Modified: 2021-07-24
Category: Cocktail
Tags: spanisch, Wein
Slug: sangria
Authors: Patrick Weber
Lang: de

## Welchen Wein verwendet man für Sangria?
Sangria soll intensiv, fruchtig aber nicht zu süß, und leicht süffig schmecken. Wähle deshalb am besten einen kräftigen, halbtrockenen Rotwein mit maximal 12 % Alkohol. Der Wein sollte außerdem nicht zu lange im Eichenfass gereift sein, weil diese dadurch ein leicht holziges Aroma erhalten, welches den Geschmack der Sangria negativ beeinflussen würde. (z.b. Lambrusco, andere spanische Rebsorten oder Valpolicella bzw. Corvina.

## Sangria - Zutaten für 4-6 Personen
* 3 unbehandelte Orangen
* 3 Äpfel
* 50 g Zucker
* 50 ml Orangenlikör
* 50 ml Brandy (alternativ Rhum Agricole)
* 1,5 l spanischer Rotwein
* Eiswürfel
* Minze zum Garnieren

1. Orangen waschen und in Scheiben schneiden. Äpfel waschen, schälen und das Kerngehäuse herausschneiden. Anschließend in Spalten schneiden.
2. Orangen und Äpfel mischen, mit Zucker bestreuen und mit Orangenlikör und Brandy beträufeln. Etwa 30 Minuten in einem großen Glasgefäß ziehen lassen.
3. Früchte mit spanischem Rotwein auffüllen. Sangria gut kühlen. Zum Servieren wahlweise Limettenscheiben, Minze und Eiswürfel ins Glas geben. 

**Tipp**: Sangria schmeckt auch als Schorle – besonders bei hochsommerlichen Temperaturen! Dazu einfach 1 l eiskaltes Mineralwasser hinzugießen.
