Title: Veganer Walnußkuchen
Date: 2021-09-29
Category: Gebäck
Tags: Vegan
Slug: walnusskuchen-vegan
Authors: Patrick Weber
Lang: de

## Zutaten
* 300g Mehl
* 250ml Milch (Soja- oder andere Plfanzenmilch)
* 200g Zucker
* 150ml Öl
* 1 Packung Backpulver
* 1 Packung Vanillezucker
* 1 Handvoll Walnüsse
* 1 Packung Kuchenglasur (Schokolade)

## Zubereitung
Die Walnüsse nicht zu fein hacken.

Alle Zutaten in eine Schüssel geben und zu einem glatten Teig verrühren. Den Teig auf ein kleines mit Backpapier belegtes Backblech streichen.

Im vorgeheizten Backofen bei 200°C ca. 25 Min. backen.

Schokoladenglasur schmelzen und den Kuchen noch heiß damit bestreichen. Abkühlen lassen. Dann mit einem scharfen Messer in kleine Quadrate schneiden.

Am besten schmeckt er, wenn er über Nacht in einem verschließbaren Behälter im Kühlschrank durchziehen konnte.
