Title: Pikanter Eistee
Date: 2021-07-07
Modified: 2021-07-07
Category: Cocktail
Tags: alkoholfrei, fruchtig
Slug: pikanter-eistee
Authors: Patrick Weber
Lang: de

* 2-3 Teelöffel Oleo Saccharum
* 100ml Earl Grey Tee (kalt)
* 2 Schuß Pfirsichbitter (optional)
* Grapefruitschnitz und ein Zweig Minze zur Verzierung

[Video](https://www.youtube.com/watch?v=1F1Deix4lkk) zum Rezept.

### Variationen
Earl Grey durch verschiedene andere Teesorten ersetzen.
