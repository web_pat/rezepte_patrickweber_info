Title: Grüner Spargel gebraten
Date: 2024-05-07
Category: Gericht
Tags: vegan, asiatisch
Slug: gruener-spargel-gebraten
Authors: Patrick Weber
Lang: de

## Zutaten
* 500g grüner Spargel
* 2 EL Sojasauce
* 1 Spritzer Worcestershiresauce
* 1 EL Ingwer, fein gerieben
* 2 Zehe/n Knoblauch, durchgepresst
* Öl, zum Braten
* Öl (Sesamöl), aus geröstetem Sesam, zur Aromatisierung

## Zubereitung
Grüner Spargel schräg in Stücke schneiden und in Öl leicht anbraten. Die Zutaten mischen und zum Spargel geben. Alles zusammen unter dem Deckel garen. Grüner Spargel sollte noch Biß haben. Mit dem Sesamöl aromatisieren.

Als Beilage oder als Gericht mit Reis servieren.

## Variationen
Falls Worcestersauce nicht verfügbar ist, durch eine Mischung aus Sojasauce, Essig, etwas Zucker und Fischsauce ersetzen.
