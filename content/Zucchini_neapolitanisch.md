Title: Zucchini neapolitanisch
Date: 2021-07-24
Modified: 2021-07-24
Category: Gericht
Tags: Italienisch
Slug: zucchini-neapolitanisch
Authors: Patrick Weber
Lang: de

![Zucchini neapolitanisch](./images/Zucchini_neapolitanisch.webp)

## Zutaten
* 4 große Zucchini
* Salzwasser
* 2 Zwiebeln
* 3 EL Olivenöl
* 500g Hackfleisch vom Rind
* 1 Packung Kochschinken in Scheiben (200g)
* 1 Bund Petersilie
* 1 Ei
* 200g geriebener Käse, Parmesan
* Salz und Pfeffer
* 50g Butter
* Tomatenketchup oder Tomatenmark
* 200ml Sahne oder veganes Creme fraiche
* Tabasco (oder Sambal Ölek)

## Zubereitung
Die Zucchini waschen, Stängelansätze rausschneiden und längs halbieren. Mit der Schnittfläche nach unten in das gesalzene, sprudelnd kochende Wasser geben und 10 Minuten zugedeckt kochen lassen.

Die Zucchini rausnehmen, abtropfen lassen und mit einem Teelöffel das Fruchtfleisch zum Teil herausschaben und würfeln. In eine Schüssel geben.

Für die Füllung die Zwiebeln schälen und fein hacken. Das Olivenöl in einer Pfanne erhitzen und die Zwiebeln ca. 2 Minuten bei schwacher Hitze hellgelb braten. Das Rinderhackfleisch dazugeben und unter gelegentlichem Rühren noch etwa 5 Minuten braten.

Die Schinkenscheiben in kleine Würfel schneiden. Die Petersilie abbrausen und hacken. Alles zusammen mit dem Ei und dem Käse in die Schüssel zum Fruchtfleisch geben und gut mischen, mit Salz und Pfeffer würzen. Eine große oder 2 kleine feuerfeste Formen mit etwas Butter einfetten, die Zucchini hineinlegen und mit der Mischung füllen.

Die Sahne mit dem Tomatenketchup (oder Tomatenmark) verrühren. Es muß eine rosafarbene, etwas dickliche Sauce entstehen. Dann mit Tabasco abschmecken - Schärfegrad je nach Geschmack. Das ganze über die Zucchini gießen und die Butter in Flöckchen darauf setzen.

Die Form in den vorgeheizten Ofen (Ober-/Unterhitze: 200°C, Umluft 180°C) auf die mittlere Schiene schieben und 20 Minuten garen. Als Beilage passt Reis.

### Variationen
* Sambal Ölek statt Tabasco in der Sauce.
* Tomatenmark in der Sauce anstatt Ketchup.
* Weniger Fleisch. 200-300g Rinderhackfleisch anstatt 500g. War selbst mit sehr großen Zuchhinis noch genügend.
* Keine Butter verwendet um das Rezept etwas fettarmer zu machen.
* Vegane Sahne anstatt Sahne.

