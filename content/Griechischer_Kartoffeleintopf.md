Title: Griechischer Kartoffel-Gemüseintopf
Date: 2023-02-18
Modified: 2023-02-18
Category: Gericht
Tags: griechisch
Slug: griechischer-kartoffel-gemueseintopf
Authors: Patrick Weber
Lang: de

## Zutaten
* 2 EL Olivenöl
* 2 Zwiebeln
* 4 Knoblauchzehen
* 2 TL Tomatenmark
* 50g Speck
* 100g Bohnen (frisch oder tiefgekühlt)
* 2 große Kartoffeln
* 1 Paprikaschote, gelb
* 1 Paprikaschote, rot
* 1 Tomate
* 3 EL Weißwein
* 1 TL Thymian
* 1 Lorbeerblatt
* Salz und Pfeffer
* Petersilie

## Zubereitung
Öl im Topf erhitzen.

Zwiebeln abziehen und fein würfeln. Knoblauch schälen und grob hacken. Beides im Öl glasig anschwitzen. Tomatenmark zugeben und kurz mitrösten. Speck würfeln und zufügen. Zuerst die grünen Bohnen, dann die Kartoffeln in den Topf füllen. Paprikaschoten und Tomate putzen bzw. schälen, dann würfeln und dazugeben. Ab und zu umrühren und alles mit Wein ablöschen. Thymian und Lorbeerblatt dazugeben und knapp mit Wasser aufgießen. Salzen, pfeffern und zugedeckt ca. 20 - 30 Minuten sanft kochen (je nachdem, wie bissfest man das Gemüse mag).

Petersilie waschen und hacken. Am Ende den Eintopf abschmecken, mit Petersilie bestreuen und schmecken lassen.

Tipp: Lässt sich übrigens durch diverse Gemüsesorten ergänzen und verändern, z. B. Möhren.
