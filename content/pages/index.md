Title: Nur Rezepte, keine Werbung
save_as: index.html

Heute schon gegessen? Keine Ahnung was es zum Essen geben soll?  
Diese einfache Rezepteseite gibt Auskunft was und wie - ohne Brimborium, ohne überflüssiges Gesabbel geht es direkt zum Eingemachten!

Wähle eine *Kategorie*, *Tag* oder das *Archiv* für eine Auflistung aller vorhandenen Rezepte.
