Title: "Drunken Noodles" mit Tofu
Date: 2022-07-28
Category: Gericht
Tags: thailändisch, Nudeln
Slug: drunken-noodles-tofu
Authors: Patrick Weber
Lang: de

Ein Klassiker der thailändischen Garküche mit einem extra Schritt für knusprigen Tofu.

## Zutaten
### Sauce
* 2,5 Esslöffel Austernsauce
* 2 Esslöffel Sojasauce
* 3 Esslöffel süße Sojasauce
* 1,5 Esslöffel Reisessig
* 2 Esslöffel Fischsauce

### Bratnudeln
* 150g flache, breite Reisnudeln
* 1 mittelgroße Zwiebel, in Streifen
* 1 rote Paprika, in Spalten geschnitten
* 2 Knoblauchzehen
* 1 oder 2 Chillischoten, fein gehackt
* 1 Handvoll Thaibasilikum
* 200g Tofu, fest

## Zubereitung
* Zutaten für die Sauce in einer kleinen Schüssel vermischen. Beiseite stellen.
* In einem Topf Reisnudeln kochen.
* Wok stark erhitzen, dann zwei Esslöffel Pflanzenöl hineingeben. Sobald es heiß genug ist Zwiebeln und Paprika hinzufügen. Anbraten bis es zart ist, etwa 4-5 Minuten. Dann Knoblauch, Chilli und Thaibasilikum dazugeben.
* Hitze reduzieren und Sauce hineingeben. Gut rühren und die Nudeln abgeseihten Nudeln hinzugeben. Gut rühren und mit der Sauce vermischen. Schließlich Tofu hinzugeben und gleichmäßig verteilen.

## Variationen
* Tofu vorher mit Stärke knusprig und goldbraun braten und dann erst zum Gericht hinzufügen.
