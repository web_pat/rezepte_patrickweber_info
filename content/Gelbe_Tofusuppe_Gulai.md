Title: Tofu gelbe Kokosmilchsuppe (Gulai Tofu)
Date: 2021-07-12
Modified: 2021-07-12
Category: Gericht
Tags: vegan, asiatisch, chinesisch
Slug: gelbe-tofusuppe-gulai
Authors: Patrick Weber
Lang: de

## Zutaten
* 400gr frisches Tofu
* 165ml Kokosmilch
* 400ml Wasser
* Öl

### Gewürze A:
* 4 Schalotten
* 2 Knoblauchzehen
* 1,5 Fingerhut Ingwer
* 1 Fingerhut Kurkuma
* 1/4 Kümmel, ganz
* 5 Nelken

### Gewürze B:
* 1 Kaffir-Limetten Blatt (siehe Detail) 
* 2 Tomaten, klein schneiden 
* 1 Zitronengras, klopfen 
* 1/2 TL Pfeffer gemahlen 
* 1/2 TL Muskatnuss gemahlen 
* 1/2 TL Koriander gemahlen 
* 1,5 Salz 

## Zubereitung
1. Tofu in kleinen Würfeln (1x1cm) schneiden, mit ein bisschen Öl braten bis sie eine hell-braune Haut bilden, abkühlen lassen. 
2. Gewürze A in einem Mörser oder mit Drehwolf zermahlen, zusammen mit Gewürze B in 4 TL Öl braten bis die Tomaten weich sind, und sie gut riechen. 
3. Gebratenes Tofu und Wasser dazu geben, ab und zu umrühren, bei niedrige Hitze schmoren für ca. 5 Minuten. 
4. Kokosmilch dazu geben und immer umrühren. Unter rühren nicht mehr kochen lassen. 
