Title: Lablabi
Date: 2023-02-25
Modified: 2023-03-01
Category: Gericht
Tags: tunesisch, vegan
Slug: lablabi
Lang: de

![Lablabi](./images/Lablabi.webp)

## Zutaten
* 2 Dosen Kichererbsen
* ein halber Laib Brot
* Olivenöl
* 1 kleine Zwiebel
* 3-4 Knoblauchzehen
* Meersalz
* 1 TL Kümmel
* 0,5 TL Koriander
* 0,5 TL Paprika, gemahlen
* 1 TL Harissapaste
* 2 Zitronen
* 0,5 Tassen grob gehackter Petersilie + etwas für Garnitur
* 2 Stängel Frühlingszwiebeln

## Zubereitung
Kichererbsen in einer mittelgroßen Pfanne bei mittlerer Hitze erwärmen, mit Flüssigkeit beigeben und mit Wasser bedecken.
Köcheln lassen und Hitze herabsetzen und mit einem Deckel abdecken, dabei einen Schlitz offen lassen. Auf diese Weise bei geringer Hitze 30 Minuten köcheln lassen bis die Kichererbsen zart werden.

Brot in etwas mehr als bißgroße Teile schneiden, mit etwas Olivenöl beträufeln und in der Pfanne antoasten bis es sich goldbraun verfärbt.

Knoblauch und Zwiebeln in einer Pfanne mit Olivenöl glasig anbraten. Dazu eine Prise Meersalz, Kümmel, Koriander und Paprikapulver.

Fertiges Knoblauch- und Zwiebelgemisch über die fertigen Kichererbsen geben. Einen Teelöffel Harissa hinzufügen, eine Prise Salz und Saft aus einer Zitrone. Dazu Petersilie und einen ordentlichen Spritzer Olivenöl. Alles zusammen gut umrühren. Abschmecken und bei Bedarf nachwürzen.

Die verbliebene Zitrone in Keile für die Garnitur. Angerichtet wird mit dem getoasteten Brot zuerst und das Lablabi wird darauf ausgebreitet. Garnieren mit Gewürzen, Olivenöl, Harissa, Petersilie und gehackten Frühlingszwiebeln. Mit den geschnittenen Zitronenstücken anrichten.
