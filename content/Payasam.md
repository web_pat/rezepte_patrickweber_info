Title: Payasam
Date: 2021-09-18
Modified: 2021-10-23
Category: Gericht
Tags: indisch, historisch, süß
Slug: payasam
Authors: Patrick Weber
Lang: de

Originalrezept aus dem 12. Jahrhundert des indischen Königs Manasollasa.

![Payasam](./images/Payasam.webp)

## Zutaten
* eine halbe Tasse kurzer Reis (wie Risottoreis)
* 1l Milch
* 1 Tasse Palmzucker oder eine halbe Tasse Zucker
* 5 Schoten Kardamom oder 3/4 Teelöffel Kardamompulver
* 10-15 Saffranfäden

## Zubereitung
1. Reis für 20-30 Minuten in Wasser aufweichen, dann Wasser abgießen.
2. Milch in eine Pfanne geben und auf mittlerwer Hitze aufwärmen bis sie leicht zu kochen beginnt. Auf mittlere Hitze abkühlen lassen, den Reis hinzugeben und rühren. Für 15 Minuten ziehen lassen und alle paar Minuten umrühren, damit der Reis nicht anbrennt.
3. Sobald der Reis weich genug ist um zwischen den Fingern zerdrückt zu werden, Palmzucker oder Zucker und Gewürze hinzufügen und rühren bis alles gründlich vermengt ist. Für weitere 5 Minuten köcheln lassen.
4. Topf von der Herdplatte nehmen und abkühlen lassen.

## Variationen
* Pflanzenmilch anstelle von Milch. Mandelmilch passt sehr gut.
* Milch reduzieren im Verhältnis zum Reis. Zwei Tassen bis etwa ein halber Liter reicht für diese Menge in der Regel aus.
* Zucker reduzieren. Auch diese Menge scheint im Verhältnis zum Reis überhöht. Etwa eine halbe Tasse anstelle einer ganzen.
* Mengen überdenken: getestet mit zweieinhalb Tassen Milch auf eine halbe Tasse Reis. Palmzucker etwa eine Vierteltasse.
