Title: Grünes Gemüsecurry
Date: 2023-07-27
Category: Gericht
Tags: vegan, asiatisch, thailändisch
Slug: gruenes-gemuesecurry
Authors: Patrick Weber
Lang: de

Zutaten für 4-6 Personen

## Zutaten
* 250 g grüne Bohnen
* 250 g Zuckerschoten
* 2 Paprikaschoten, grün
* 4 spitze gelbe Paprikaschoten
* 1 Pfefferschote, grün
* 1 Bund Frühlingszwiebeln
* 4 junge Karotten
* 3 Knoblauchzehen
* 1 Stück frische Ingwerwurzel
* 5 Eßlöffel Erdnuß- oder Sojaöl
* 1 Teelöffel helle Senfkörner
* 1 Teelöffel geschrotener Koriander
* 100 g Kokosflocken
* ¼ l Gemüsebrühe
* Salz
* 1 Teelöffel Zucker
* 150 g Sahnejoghurt
* 1 – 2 Eßlöffel Currypulver

## Zubereitung
1. Die Bohnen und die Zuckerschoten abfädeln, waschen und abtropfen lassen. Bohnen einmal brechen.
2. Die grünen Paprikaschoten an den Nahtstellen aufschneiden, von Stengelansätzen und Samensträngen befreien. Die gelben Paprikaschoten und die Pfefferschote längs halbieren, die Stengelansätze abschneiden, die Samenstränge und die Kerne sorgfältig entfernen.
3. Alles abspülen und gründlich abtropfen lassen. Grüne und gelbe Paprika in feine Streifen schneiden, die Pfefferschote sehr fein würfeln.
4. Die Frühlingszwiebeln putzen, die weißen Knollen fein würfeln, die grünen Teile schräg in dünne Ringe schneiden. Die Möhren schälen oder schaben und sehr fein würfeln. Die Knoblauchzehen und den Ingwer schälen und hacken.
5. Das Öl im Wok erhitzen. Den Ingwer und den Knoblauch mit den Senfkörnern und dem Koriander hineingeben und unter Rühren bei nicht zu starker Hitze rösten, bis der Knoblauch glasig ist. Er darf nicht braun werden, dann wird er bitter.
6. Mit den Bohnen beginnend das Gemüse portionsweise ins Öl geben und jeweils etwa 30 Sekunden andünsten, dann zur Seite schieben und die nächste Portion in den Wok geben. Wenn alles Gemüse verbraucht ist, die Kokosflocken unterrühren und das Gemüse mit der Gemüsebrühe ablöschen.
7. Das Gemüse unter häufigem Wenden 6 – 7 Minuten garen. Es soll auf jeden Fall noch Biß haben. Erst jetzt mit Salz und Zucker würzen. Den Wok vom Herd nehmen und den Joghurt unter das Gemüse rühren. Das fertige Gericht kräftig mit dem Curry abschmecken. Dazu schmeckt Reis.

## Variationen
* Sahnejoghurt durch vegane Creme ersetzen.
