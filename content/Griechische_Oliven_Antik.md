Title: Griechische Oliven nach antiker Art
Date: 2021-07-24
Modified: 2021-07-24
Category: Beilage
Tags: griechisch, historisch
Slug: griechische-oliven-antik
Authors: Patrick Weber
Lang: de

## Zutaten
* 2 Becher oder 300g Oliven, entsteint
* 1/4 Tasse oder 60ml Olivenöl
* 2 Esslöffel Rotweinessig
* 1 Esslöffel gehackter Koriander
* 1/2 Teelöffel Kümmel
* 1 Teelöffel Fenchel
* 2 Teelöffel Weinraute
* 1 Teelöffel Pfefferminz

## Zubereitung
Oliven hacken. Alle anderen Kräuter und Zutaten mischen und anschließend über die Oliven träufeln. Gut verrühren und ziehen lassen. Anrichten mit Brot und Käse.
