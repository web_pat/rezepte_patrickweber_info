Title: Pizzasauce
Date: 2023-02-18
Modified: 2023-02-18
Category: Beilage
Tags: italienisch
Slug: Pizzasauce
Authors: Patrick Weber
Lang: de

## Zutaten
* 250g Tomaten, passiert
* 2 EL Tomatenmark
* Zwiebel, klein, fein gehackt
* 1 Knoblauchzehe, zerdrückt
* Basilikum
* Oregano
* Rosmarin
* Olivenöl
* Zucker
* Meersalz
* Pfeffer

## Zubereitung
Für die Pizzasauce die Zwiebel in heißem Öl glasig werden lassen. Die passierten Tomaten dazu geben und aufkochen lassen. Die restlichen Zutaten jetzt dazu geben und für ca. 20-25 min leicht köcheln lassen.
