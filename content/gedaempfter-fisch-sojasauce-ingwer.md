Title: Gedämpfter Fisch mit Sojasauce und Ingwer
Date: 2024-04-30
Category: Gericht
Tags: asiatisch, thailändisch
Slug: gedaempfter-fisch-sojasauce-ingwer
Authors: Patrick Weber
Lang: de

Thailändisch: Plaa Neung See Ew

## Zutaten
* getrocknete Shiitake Pilze
* 4 EL Helle Sojasauce
* 1 TL Zucker
* 1 ganzer Fisch (Dorade, Wolfsbarsch o.ä.), ausgenommen und entschuppt
* 2 Knoblauchzehen
* 2 daumengroßes Stück Ingwer
* 0.5 Bund Selleriekraut
* 2 Frühlingszwiebeln
* 4 EL Hühnerfond
* 1 Prise Schwarzer Pfeffer, gemahlen

## Zubereitung
1. Getrocknete Shiitake-Pilze in kaltem Wasser 1 Stunde quellen lassen. Das Einweichwasser wegschütten und die Pilze 15 Minuten in Wasser leicht kochen, wobei  sie knapp bedeckt sein sollten. 1 Esslöffel Sojasauce, eine Prise Salz und Zucker hinzufügen.

2. Fisch innen und außen unter kaltem Wasser abspülen. Mit Küchenkrepp trockentupfen und auf ein Schneidebrett legen.

Den Fisch mit einem scharfen Messer auf beiden Seiten mit mehreren Schnitten quer einschneiden. Die Schnitte sollten vom Kopf beginnend im Abstand von 2cm bis zur Schwanzflosse gehen und bis zur Mittelgräte reichen, ohne diese zu durchtrennen.

Damit erreicht man ein gleichmässigeres Garen des Fisches.

3. Die Knoblauchzehen schälen, halbieren und mit der flachen Seite eines schweren Küchenmessers leicht zerdrücken.

Den Ingwer schälen und in feine Stifte schneiden.

Den Sellerie waschen und grob hacken, dabei einige Blättchen zur Dekoration beiseite legen.

Frühlingszwiebeln in feine Streifen schneiden.

4. Esslöffel Sojasauce, 4 Esslöffel von dem Pilzsud und die Hühnerbrühe über den Fisch gießen.

Pilze aus dem restlichen Sud nehmen und in Scheiben schneiden. Zusammen mit dem Knoblauch, dem Ingwer, dem Sellerie und den Frühlingszwiebeln den Fisch garnieren.

Den Dampfgarer aufheizen und die Schale mit dem Fisch hineingeben. Je nach Größe zwischen 12-18 Minuten dämpfen. Der Fisch ist gar, wenn das Fleisch hinter dem Kopf, an der dicksten Stelle, weißlich gefärbt ist.

Mit den restlichen Sellerieblättern bestreuen und servieren. Dazu reicht man gedämpften Jasminreis.

## Variationen
Falls getrocknete Tomaten in Öl eingelegt sind, vor dem Zugeben in die Pfanne auf Küchenpapier ausbreiten um überschüssiges Öl zu entfernen.

![Gedämpfter Fisch mit Sojausauce und Ingwer](./images/gedaempfter-fisch-sojasauce-ingwer.webp)
