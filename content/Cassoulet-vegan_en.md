Title: Cassoulet (vegan)
Date: 2021-12-31
Category: Gericht
Tags: französisch
Slug: cassoulet-vegan
Lang: en

This very vegan cassoulet is a delicious stew of vegetables and white beans. This recipe is gluten-free and low-fat.

## Ingredients 
* 1 cup dry white beans, or 3 cups canned, drained and washed. If using dry beans, soak overnight and cook until tender. (cannelini or great northern or navy beans will do)
* 8 oz package of tempeh, cubed into ½-inch pieces
* 1 tablespoon extra virgin olive oil
* 1 medium onion, diced
* 2 medium carrots, sliced
* 2 stalks of celery, diced
* 5 cloves garlic, finely minced, or crushed into a paste
* 15 mushrooms sliced (use any kind-- wild mushrooms would be great here. I used crimini because I always have them on hand)
* 2 tomatoes diced
* 1 tablespoon chopped sage
* 1 tablespoon chopped thyme
* 2 bay leaves
* 2 tablespoon parsley, chopped
* ½ cup red wine, optional
* Salt and ground black pepper to taste

## Instructions
Heat oil in a large saucepan.  
Add the tempeh cubes and saute, stirring frequently, until the tempeh is golden-brown.  
Add the onions, garlic, celery and carrots. Add a pinch of salt and some ground black pepper and saute the vegetables about five minutes until the onions are translucent.  
Add the mushrooms and the wine. Cook until the moisture from the vegetables and all of the water has evaporated.  
Add the tomatoes and beans along with the bay, sage, parsley and thyme.  
Bring to a boil -- add water if the stew is too thick - and turn the heat to a level where it boils gently.  
Check salt and pepper. Turn off the heat and serve hot with some crusty French bread.
