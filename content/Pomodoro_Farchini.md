Title: Pomodori Farcini all'Erbette
Date: 2021-09-18
Category: Gericht
Tags: italienisch
Slug: pomodori-farcini-erbette
Authors: Patrick Weber
Lang: de

Petersilie, Zwiebeln, Sauerampfer und Minze hacken. Schinken hinzufügen und mit Salz und Pfeffer abschmecken. Eigelb und geriebenen Provatura hinzufügen. Die Mischung in die Tomaten einfüllen, anbraten und mit der Brühe des Schinkens anrichten.

## Zutaten
Abhängend von Anzahl und Größe der zu füllenden Tomaten.
* Tomaten
* Petersilie
* Zwiebeln
* Sauerampfer (oder vergleichbares, grünes Gemüse wie Rukola oder Mangold)
* Pfefferminz
* Kochschinken
* Eigelb (eines je 2 Tomaten)
* Provatura Käse (oder frischer Mozzarella)
* Salz
* Pfeffer
* Olivenöl

## Zubereitung
1. Mince the herbs and onion and mix together in a medium bowl.
2. Tear the prosciutto into small pieces, then mix into the herbs and add salt and pepper to taste. Grate the Provatura into the mixture and finally add the egg yolks. Mix together until you form a paste.
3. Slice off the tops of the tomatoes and scoop out the inside with a spoon. Then stuff the tomatoes with the herb mixture until full.
4. Add olive oil to a frying pan and set over medium heat. Fry the tomatoes, first on the bottom for 2-3 minutes, then flip them over and fry the tops for 2-3 minutes. If you wish for the egg to be fully cooked, place frying pan into an oven set at 350°F/175°C for 10-12 minutes or until the center of the filling registers 165°F/74C. If you wish, add additional cheese to the tops of the tomatoes before placing them in the oven.
5. Remove the pan from the oven and plate the tomatoes garnishing with herbs.

1. Kräuter und Zwiebeln hacken und gemeinsam in eine mittelgroße Schüßel geben.
2. Kochschinken in kleine Streifen oder Würfel schneiden, dann den Kräutern beimischen und Salz und Pfeffer hinzufügen. Provatura Käse hineinreiben und schließlich das Eigelb hinzufügen. Alles zusammenrühren bis sich eine Art Aufstrich ergibt.
3. Die Spitze der Tomaten abschneiden und den Inhalt mit Löffel aushöhlen. Dann die Tomaten mit der Mischung anfüllen.
4. Olivenöl in eine Pfanne geben und über mittlerer Hitze erhitzen. Die Tomaten zuerst von unten für 2-3 Minuten anbraten, dann umdrehen und die andere Seite anbraten. Falls das Eigelb vollständig durchgekocht werden soll die Tomaten mit der Pfanne oder einer feuerfesten Form für 10-12 Minuten in den Ofen bei 180° geben. Je nach Geschmack kann auch eine weitere Käsesorte über die Tomaten gerieben werden, bevor sie in den Ofen kommen.
5. Tomaten herausnehmen und mit Kräutern verziert anrichten.
