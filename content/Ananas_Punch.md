Title: Ananas Punch
Date: 2021-07-07
Modified: 2022-07-22
Category: Cocktail
Tags: alkoholfrei, fruchtig, Rhum
Slug: ananas-punch
Authors: Patrick Weber
Lang: de

* 60ml Ananas Shrub (pineapple shrub)
* 60ml Grapefruitsaft
* Garnitur - Ananasstücke, Minzzweige

[Video](https://www.youtube.com/watch?v=1F1Deix4lkk) zum Rezept.

### Variationen
Auch gut als Punch mit Rhum, dann offensichtlich nicht mehr alkoholfrei.
