Title: Gebratenes Wokgemüse in süß-saurer Sauce
Date: 2021-10-11
Category: Gericht
Tags: vegan, asiatisch, thailändisch
Slug: gebratenes-gemuese-suess-sauer
Authors: Patrick Weber
Lang: de

Auf thailändisch: priaw wan phak

## Zutaten
* 150g Tofu, fest. Geschnitten und golden gebratenes
* 3 Esslöffel Plfanzenöl
* 1 Esslöffel gehackter Thaiknoblauch (oder normaler Knoblauch)
* 100g Zwiebeln, in Spalten geschnitten
* 200g Wassergurken, in Scheiben geschnitten
* eine halbe Ananas (oder eine Dose), gewürfelt
* 100g Tomaten, in Spalten geschnitten
* 75g Paprika, grün
* 90g Paprika, rot
* 2-3 Esslöffel Wasser
* 1 Teelöffel Tomatenmark
* 1 Teelöffel Sojasauce, hell
* 2 Esslöffel Essig
* 1 Esslöffel Zucker
* 2 Zweige Frühlingszwiebeln

## Zubereitung
Pfanne bei mittlerer Hitze vorheizen. Öl und dann Knoblauch hineingeben und goldgelb anbraten. Zwiebeln hinzugeben und braten bis glasig und eingeweicht. Gurken, Ananas, Tomaten und Paprika hinzugeben und unter ständigem Rühren anbraten. Bei Bedarf etwas Wasser hinzugeben und nacheinander den Tofu, Tomatenmark, Sojasauce, Essig und Zucker hinzugeben und ständig rühren.  
Frühlinszwiebeln hinzufügen und rühren.

Zur Garnierung können etwas gelbbraun angebratene Knoblauchwürfel darauf versprenkelt werden.

## Variationen
* Tofu kann auch mit getrockneter Tofuhackmasse ersetzt werden. Diese zuvor einweichen und das Wasser gut ausquetschen, bevor es in die Pfanne kommt.
* Essiggurken aus dem Glas als Ersatz für frische Wassergurken. Dann etwas sparsamer mit der Zugabe des weiteren Essig sein, da die Gurken bereits Säure hineinbringen.
* Weißkohl passt sehr gut in dieses Gericht und drängt sich nicht in den Vordergrund. Am besten in kleine Streifen geschnitten.
