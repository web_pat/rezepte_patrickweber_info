Title: Royal Bermuda Yacht Club
Date: 2021-07-24
Modified: 2021-07-24
Category: Cocktail
Tags: Rhum
Slug: royal-bermuda-yacht-club
Authors: Patrick Weber
Lang: en

* 50ml (1 & 2/3oz) rum (I actually prefer a full bodied white rum, to keep it brighter and fruitier, but the original recipe is with an aged rum which gives a richer cocktail)
* 20ml (3/4oz) fresh lime juice
* 15ml (1/2oz) falernum*
* 5ml (1/4oz) curacao
* Lime wheel to garnish

* A jigger
* Shaker tins
* A hawthorn strainer
* A fine strainer
* A coupe glass

It is possible to make falernum, but it is a little fiddly. This is a good DIY [recipe](https://punchdrink.com/recipes/falernum/).

Add all of your ingredients to your shaker tins and fill full with ice. Seal the tins together and shake as hard as you can. Pop the tins open and double strain, using the hawthorn strainer to hold the ice back in the tin and pouring through the fine strainer in to your coupe glass. Garnish with a lime wheel, and enjoy!
