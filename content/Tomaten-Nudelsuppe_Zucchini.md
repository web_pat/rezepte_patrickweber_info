Title: Tomaten-Nudelsuppe mit Zucchini
Date: 2021-08-17
Category: Gericht
Tags: Nudeln, einfach
Slug: tomaten-nudelsuppe-zucchini
Authors: Patrick Weber
Lang: de

![Tomaten-Nudelsuppe Zucchini](./images/Tomaten-Nudelsuppe_Zucchini.webp)

## Zutaten
* 2 Zwiebeln
* Öl
* 100g Bauchspeck oder Speckwürfel
* 1 Dose (400-500g) Tomaten oder frische Tomaten
* 750ml Gemüsebrühe
* 150g Nudeln
* 1 Zucchini
* 4 Zehen Knoblauch, gehackt
* 1 EL Honig
* Salz
* Pfeffer
* Chilipulver oder Cayennepfeffer
* Basilikum

## Zubereitung
Die feingehackte Zwiebel in etwas Öl anrösten. Den würfelig geschnittenen Bauchspeck zugeben und kurz mitrösten lassen. Die Tomatenstückchen und die Gemüsebrühe zugeben. Die Zucchini in ca. 1 cm große Würfel schneiden und mit den Nudeln untermischen. Mit Knoblauch, Honig, Salz, Pfeffer und evtl. Chilipulver würzig abschmecken und für ca. 10 Minuten köcheln lassen. Zwischendurch immer wieder etwas umrühren. Zum Schluss noch das gehackte Basilikum untermischen.

Auf Teller verteilen und nach Belieben mit Baguette servieren.
