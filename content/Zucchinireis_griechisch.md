Title: Griechischer Zucchinireis
Date: 2021-08-15
Modified: 2021-08-15
Category: Gericht
Tags: griechisch, Reis
Slug: zucchinireis-griechisch
Authors: Patrick Weber
Lang: de

![Griechischer Zucchinireis](./images/Zucchinireis_griechisch.webp)

## Zutaten für 2 Personen

* einige Esslöffel Olivenöl
* 1-2 Zwiebeln
* 500g Zucchini
* 0,5 Tasse Reis, nicht vorgekocht
* 200g gestückelte Tomaten
* Salz
* 1 Teelöffel Minze
* eine Prise Zucker
* 2 Teelöffel Dill, gehackt
* Pfeffer, gemahlen
* Zitrone oder Zitronensaft

## Zubereitung
Olivenöl erhitzen bis es leicht simmert. Zwiebeln hinzufügen und anbraten bis sie glasig werden. Zucchini hinzufügen und für 1-2 Minuten gut umrühren. Reis hinzufügen und sachte rühren bis der Reis gut mit Olivenöl getränkt ist.

Tomaten hinzufügen und eine Prise Zucker darüber geben. Dazu trockene Minze und eine viertel Tasse Wasser hinzugeben. Sachte rühren bis das Wasser simmert. Bei geringer Hitze abdecken und für weitere 20 Minuten köcheln lassen. Nach und nach jeweils zwei Esslöffel Wasser hinzugeben und den Reis quellen lassen.

Sobald der Reis durch ist vom Herd nehmen, gehackten Dill hinzufügen, Salz und gemahlenen Pfeffer. Gut umrühren und noch einmal abgedeckt für 5 Minuten stehen lassen. Anrichten mit einem Spritzer Zitronensaft und je nach Geschmack mit etwas Feta.
