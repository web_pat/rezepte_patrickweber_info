Title: Spaghetti mit Cocktailtomaten
Date: 2021-07-07
Modified: 2021-07-24
Category: Gericht
Tags: italienisch, Pasta
Slug: spaghetti-cocktailtomaten
Authors: Patrick Weber
Lang: de

![Spaghetti mit Cocktailtomaten](./images/Spaghetti_Cocktailtomaten.webp)

Halbierte Tomaten in eine große Schüssel geben, kräftig salzen und pfeffern. Parmesan darüber geben und den gepressten Knoblauch. Pinienkerne drüber verteilen, danach das Basilikum darüber streuen. Olivenöl darüber träufeln. Die Tomaten am besten über Nacht ziehen lassen.

Wasser aufsetzen, die Spaghetti bissfest kochen und heiß unterheben und genießen!

Die "Soße" erwärmt sich nur durch die Spaghetti. Ist ein leckeres Essen an heißen Tagen und lässt sich auch für Besuch gut vorbereiten.

**Arbeitszeit**: ca. 30 Minuten
**Koch-/Backzeit**: ca. 10 Minuten
**Ruhezeit**: ca. 6 Stunden

## Zutaten
* 700g Cocktailtomaten, Halbierte
* Salz und pfeffer
* 250g Parmesan, gerieben
* 2 Knoblauchzehen, gepresst oder gehackt
* 100 g Pinienkerne, ohne Fett geröstet
* 1 Bund Basilikum
* 6 EL Olivenöl
* 500g Spaghetti

## Variationen
* Pinienkerne durch rotes Pesto ersetzen.

* Optional gebratene Garnelen hinzufügen.
