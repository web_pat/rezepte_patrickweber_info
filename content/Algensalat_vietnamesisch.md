Title: Vietnamesischer Algensalat
Date: 2021-07-25
Modified: 2021-07-25
Category: Beilage
Tags: Salat, vietnamesisch, asiatisch
Slug: algensalat-vietnamesisch
Authors: Patrick Weber
Lang: de

## Zutaten
* Frische Algen
* Ananas, gestückelt aus der Dose
* Sojasauce
* Erdnüsse
* Chili, rot

## Zubereitung
Algen und gestückelte Ananas in eine Schüssel geben und gut vermischen. Chilischote klein hacken und gut untermischen. Erdnüsse klein hacken und darüber streuen. Zuletzt mit Sojasauce abschmecken und kalt servieren.
