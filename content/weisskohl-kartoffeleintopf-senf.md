Title: Weißkohl-Kartoffeleintopf mit Senf
Date: 2022-04-29
Modified: 2022-04-29
Category: Gericht
Tags: vegan, Eintopf
Slug: weisskohl-kartoffeleintopf-senf
Authors: Patrick Weber
Lang: de

## Zutaten
* ½ Kopf Weißkohl
* 1 mittelgroße Zwiebel
* 4 Kartoffel(n), festkochend
* 1 EL,gehäuft Dijon Senf
* 1 Liter Gemüsebrühe
* 1 TL Kümmelpulver oder Kümmel
* Paprikapulver, edelsüß
* Salz und Pfeffer
* 3 EL Öl
* 30 ml Pflanzensahne (Pflanzencreme Cuisine)

## Zubereitung
Vom Weißkohl die äußeren Blätter und den Strunk entfernen. Den Kohl in Streifen schneiden. Die Zwiebel schälen und nach Belieben klein schneiden.

Das Öl in einem gusseisernen Topf erhitzen, die Zwiebel anbraten und nach kurzer Zeit den Kohl dazugeben. Mit Kümmel und Paprika würzen. Wenn das Kraut schön Farbe genommen hat, die Gemüsebrühe dazu geben, aufkochen lassen. Auf mittlerer Stufe köcheln lassen.

In der Zwischenzeit die Kartoffeln schälen und klein würfeln. Nach ca. 20 Min. die Kartoffeln zur Suppe geben und weitere 10 - 15 Min. köcheln lassen. Jetzt sollte alles weich sein. Den Senf dazugeben und nach Bedarf mit Salz und Pfeffer würzen. Zum Schluss die Sahne unterrühren.

## Variationen
* Knoblauch hinzufügen.
* Verträgt mehr Zwiebeln als angegeben.
* Falls Pflanzensahne nicht verfügbar geht auch Pflanzenmilch.
