Title: Planteur
Date: 2021-07-24
Modified: 2021-07-24
Category: Cocktail
Tags: französisch, Rhum
Slug: planteur
Authors: Patrick Weber
Lang: fr

* 70 cl de rhum blanc
* 30 cl de rhum ambré ou vieilli
* 3 Litres de jus de fruits exotiques divers
* Le jus de 3 citron verts
* Des épices (cannelle, muscade, cardamome…)
* Sirop de sucre de canne (en option)
