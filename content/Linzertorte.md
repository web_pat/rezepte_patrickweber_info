Title: Linzertorte
Date: 2022-08-20
Category: Gebäck
Tags: Kuchen
Slug: linzertorte
Authors: Patrick Weber
Lang: de

## Zutaten
* 200g Weizenmehl
* 1 gestrichener TL Backpulver
* 125g Zucker
* 1 Päckchen Vanillinzucker
* 2 Tropfen Backöl
* Bittermandel
* 1 Messerspitze gemahlene Nelken
* gemahlener Zimt
* 1 Eiweiß
* 1 Eigelb
* 125g kalte Butter
* 125g gemahlene Mandel oder Haselnußkerne
* etwa 100g Konfitüre
* 1 TL Milch
* 1 EL Kirschwasser

## Zubereitung
Das mit Backpulver gemischte Mehl auf die Tischplatte sieben. In die Mitte eine Vertiefung eindrücken. Zucker, Vanillinzucker, Backöl, Gewürze, Eiweiß und die Hälfte des Eigelbs hineingeben. Mit einem Teil des Mehls zu einem dicken Brei verarbeiten. Darauf die in Stücke geschnittene Butter geben. Die Mandeln darüber streuen, mit Mehl bedecken und von der Mitte aus allen Zutaten schnell zu einem glatten Teig verkneten. Sollte er kleben, ihn eine Zeitlang kalt stellen.

Knapp die Hälfte des Teiges zu einer Platte in der Größe einer Springform (Durchmesser etwa 28cm) ausrollen. In gleich breite Streifen rädern und den übrigen Teig auf dem Springformboden ausrollen. Mit Konfitüre bestreichen, dabei am Rand etwa 1cm frei lassen. Die Teigstreifen gitterförmig über die Konfitüre legen. Den Rand ebenfalls mit Streifen belegen.

Das restliche Eigelb mit Milch verschlagen, die Teigstreifen damit bestreichen.

Den Kuchen in den vorgeheizten Ofen bei 175-200°C geben.  
Backzeit: 25-30 Minuten.
