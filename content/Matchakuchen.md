Title: Matchakuchen
Date: 2024-07-09
Category: Gebäck
Tags: Vegan
Slug: matchakuchen
Authors: Patrick Weber
Lang: de

![Matchakuchen](./images/Matchakuchen.webp)

## Zutaten
* 4 Eier
* 150g Zucker
* 50ml Olivenöl
* 300g Mehl
* 1 TL Backpulver
* Prise Salz
* 1 Schuß Rhum
* Kardamom
* Matchapulver

## Zubereitung
Eier in den Zucker schlagen und schaumig rühren. Olivenöl und Rhum dazugeben und etwas weiter rühren. Dann Matchapulver, Backpulver und Salz hinzugeben und weiter vermengen.  
In den Ofen bei 170° und etwa 40 Minuten backen.
