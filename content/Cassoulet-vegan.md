Title: Cassoulet (vegan)
Date: 2022-08-21
Category: Gericht
Tags: französisch
Slug: cassoulet-vegan
Lang: de

Cassoulet ist ein traditioneller französischer Eintopf, hier angepasst um das das Rezept vegan zu machen. Es enthält verschiedenes Gemüse und weiße Bohnen, ist glutenfrei und fettarm.

## Zutaten
* 1 Tasse getrocknete weiße Bohnen oder 3 Tassen weiße Bohnen aus der Dose, gewaschen und abgetropft. Getrocknete Bohnen über Nacht in Wasser aufweichen
* Eine Packung Tempeh, gewürfelt in etwa 2cm große Stücke
* 1 EL Olivenöl
* 1 mittelgroße Zwiebel, gewürfelt
* 2 Karotten, in Scheiben geschnitten
* 2 Stangensellerie, gewürfelt oder in Scheiben geschnitten
* 5 Zehen Knoblauch, gehackt oder gepresst
* 15 Pilze in Scheiben (jede Art von Pilzen geht, am besten Wildpilze)
* 2 Tomaten, gewürfelt
* 1 EL Salbei, gehackt
* 1 EL Thymian, gehackt
* 2 Lorbeerblätter
* 2 EL Petersilie, gehackt
* 0,5 Tasse Rotwein (optional)
* Salz und gemahlener Pfeffer

## Zubereitung
Öl in eine Pfanne geben und erhitzen. Tempehwürfel hinzufügen und unter regelmäßigem Rühren goldbraun anschwitzen. Zwiebeln, Knoblauch, Sellerie und Karotten hinzufügen. Eine Prise Salz und etwas Pfeffer hinzufügen und Gemüse für etwa 5 Minuten andünsten, bis die Zwiebeln glasig werden.

Pilze hinzugeben und Wein eingießen. Kochen bis die Flüssigkeit des Gemüses und das Wasser verdunstet sind. Tomaten und Bohnen hinzugeben, dann die Kräuter.

Alles zusammen zum Kochen bringen und dann bei kleiner Hitze köcheln lassen. Falls die Brühe zu dickflüssig wird, etwas Wasser hinzugeben.

Abschmecken mit Salz und Pfeffer. Den Herd abstellen und heiß mit frischem Baguette servieren.
