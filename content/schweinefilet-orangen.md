Title: Schweinefilet mit Orangen
Date: 2023-07-27
Category: Gericht
Tags: Fleisch
Slug: schweinefilet-orangen
Authors: Patrick Weber
Lang: de

Zutaten für 4 Personen

## Zutaten
* 200g kleine Schalotten
* 600g Schweinefilet
* 40g Butterschmalz
* 1 Eßlöffel Mehl
* 1/8 l trockener Weißwein
* 1/8 l frisch gepresster Orangensaft
* Salz
* 2 Orangen
* 100 g Crème fraiche
* 2 Teelöffel rosa Pfefferkörner (oder andere Körner nach Belieben)

## Zubereitung
1. Die Schalotten schälen. Das Schweinefleisch von Häuten und Sehnen befreien.
2. Das Butterschmalz im Wok erhitzen und die Schalotten rundherum bräunen. Inzwischen das Fleisch schräg zur Faser in dünne Scheiben schneiden. Mit dem Mehl bestäuben.
3. Die Schalotten auf den Abtropfrost legen. Das Fleisch im Bratfett portionsweise anbraten. Fertige Portionen auch auf den Rost legen. Das Fleisch und die Schalotten wieder mischen, mit dem Wein und den Orangensaft ablöschen, salzen und im geschlossenen Wok etwa 5 Minuten schmoren.
4. Inzwischen die Orangen schälen und die Fruchtstücke aus den Trennhäuten lösen. Unter das Fleisch mischen und kurz erwärmen. Das Gericht nach Belieben mit etwas Orangenlikör und der Creme fraiche verfeinern. Mit Salz abschmecken und die Pfefferkörner unterrühren. Dazu dünne Bandnudeln reichen.

## Variationen
* Statt der Orangen können auch rosa Grapefruits verwendet werden. Sie geben dem Gericht eine herbsüße Note.
