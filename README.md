# rezepte_patrickweber_info
Lightning fast no bullshit recipe website created using [Pelican](https://getpelican.com).  
Content is created in markdown format. The theme is based on [tuxlite_tbs](https://github.com/getpelican/pelican-themes/tree/master/tuxlite_tbs) and altered to better display recipe listings rather than a regular blog.

Click [here](https://codeberg.org/web_pat/tuxlite_recipes) for a different repository containing the Pelican template used to create this website.

## ToDo
* Cosmetical changes
* Give credit to [original theme](https://github.com/getpelican/pelican-themes/tree/master/tuxlite_tbs) this website is based on.
* Add "Über" page

## License
The content of this project itself is licensed under the [Creative Commons Attribution-ShareAlike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/) (CC BY-SA 4.0), and the underlying source code used to format and display that content is licensed under the [MIT license](./LICENSE).
